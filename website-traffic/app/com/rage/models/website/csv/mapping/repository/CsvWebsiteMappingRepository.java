package com.rage.models.website.csv.mapping.repository;

import java.util.List;

import com.google.inject.ImplementedBy;
import com.rage.models.website.csv.mapping.CsvWebsiteMapping;
import com.rage.models.website.csv.mapping.repository.impl.CsvWebsiteMappingRepositoryImpl;

import io.ebean.SqlRow;

@ImplementedBy(CsvWebsiteMappingRepositoryImpl.class)
public interface CsvWebsiteMappingRepository {

	public List<SqlRow> getCsvWebsiteMappingByCsvId(String csvId);
	
	public boolean addCsvWebsiteMapping(List<CsvWebsiteMapping> csvWebsiteMappingList);
}
