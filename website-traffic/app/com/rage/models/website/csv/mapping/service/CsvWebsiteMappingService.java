/**
 * 
 */
package com.rage.models.website.csv.mapping.service;

import java.util.List;

import com.google.inject.ImplementedBy;
import com.rage.models.website.csv.mapping.CsvWebsiteMapping;
import com.rage.models.website.csv.mapping.service.impl.CsvWebsiteMappingServiceImpl;

import io.ebean.SqlRow;

/**
 * @author neethithevan.r
 *
 */
@ImplementedBy(CsvWebsiteMappingServiceImpl.class)
public interface CsvWebsiteMappingService {

	public List<SqlRow> getCsvWebsiteMapping(String csvId);
	
	public boolean addCsvWebsiteMapping(List<CsvWebsiteMapping> csvWebsiteMappingList);
}
