/**
 * 
 */
package com.rage.models.website.csv.mapping.repository.impl;

import java.util.List;

import com.rage.models.website.csv.mapping.CsvWebsiteMapping;
import com.rage.models.website.csv.mapping.repository.CsvWebsiteMappingRepository;

import io.ebean.Ebean;
import io.ebean.SqlQuery;
import io.ebean.SqlRow;

/**
 * @author neethithevan.r
 *
 */
public class CsvWebsiteMappingRepositoryImpl implements CsvWebsiteMappingRepository {

	@Override
	public List<SqlRow> getCsvWebsiteMappingByCsvId(String csvId) {
		
		System.out.println("csv id to websit data fetching...");
		String mappingtableJoinQuery="select w.id,w.url from csv_website_mapping cwm "
				+ "join website w on w.id = cwm.website_id "
				+ "where cwm.csv_id=:csvid";
		SqlQuery createSqlQuery = Ebean.createSqlQuery(mappingtableJoinQuery);
		createSqlQuery.setParameter("csvid", csvId);
		List<SqlRow> weblist = createSqlQuery.findList();
		
	/*	
		List<CsvWebsiteMapping> csvWebsiteMappingList = Ebean.find(CsvWebsiteMapping.class).fetch("website", "url").where()
				.eq("csv_id", csvId).findList();
		if (!csvWebsiteMappingList.isEmpty()) {
			return csvWebsiteMappingList;
		}*/
		
		if(weblist !=null && !weblist.isEmpty()) {
			return weblist;
		}
		
		return null;
	}

	@Override
	public boolean addCsvWebsiteMapping(List<CsvWebsiteMapping> csvWebsiteMappingList) {
		if (csvWebsiteMappingList !=null && !csvWebsiteMappingList.isEmpty()) {
			
			Ebean.saveAll(csvWebsiteMappingList);
			
			/*csvWebsiteMappingList.forEach(csvWebsiteMapping -> {
				csvWebsiteMapping.save();
			});*/
			return true;
		}
		return false;
	}

}
