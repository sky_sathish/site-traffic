/**
 * 
 */
package com.rage.models.website.repository.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.rage.models.csv.Csv;
import com.rage.models.website.Website;
import com.rage.models.website.WebsiteTemp;
import com.rage.models.website.repository.WebsiteRepository;

import io.ebean.CallableSql;
import io.ebean.Ebean;
import io.ebean.SqlQuery;
import io.ebean.SqlRow;

/**
 * @author neethithevan.r
 *
 */
public class WebsiteRepositoryImpl implements WebsiteRepository {

	@Override
	public Website getWebsiteByUrl(String websiteUrl) {
		return Ebean.find(Website.class).where().eq("url", websiteUrl).findOne();
	}

	public List<SqlRow> addWebsiteUrl(List<Website> websiteList,Csv csvDetails) {
		
		List<WebsiteTemp> webListTemp=new ArrayList<>();
		
		if (websiteList !=null && !websiteList.isEmpty()) {
			websiteList.forEach(website -> {
				WebsiteTemp web=new WebsiteTemp(website,csvDetails);
				webListTemp.add(web);
			});
			
			if(webListTemp.size()>0) {
				Ebean.saveAll(webListTemp);
			}
			
			System.out.println("Data saved to Temp Table!");
			 
			System.out.println("Temp table to Website table data coping...");
			String copyTempTable_to_WebsiteTable="INSERT INTO website  (url,alexa_ranking,country_ip,country_code_page,ip,web_server,phone,email) select  url,alexa_ranking,country_ip,country_code_page,ip,web_server,phone,email from website_temp where csv_id="+csvDetails.getId()+" ON DUPLICATE KEY UPDATE url=VALUES(url)";
		
			System.out.println(copyTempTable_to_WebsiteTable);
			CallableSql callableSqlTemp_to_Website = Ebean.createCallableSql(copyTempTable_to_WebsiteTable);
			Ebean.execute(callableSqlTemp_to_Website);
			System.out.println("Temp table to Website table data copied!");
			
			System.out.println("join query executing...");
			String WebsiteTable="select w.id,w.url,wt.csv_id " + 
					"from website_temp wt  "+ 
					"join website w on w.url=wt.url where " + 
					"wt.csv_id=:csvid";
			System.out.println(WebsiteTable);
			
			SqlQuery createSqlQuery = Ebean.createSqlQuery(WebsiteTable);
			createSqlQuery.setParameter("csvid", csvDetails.getId());
			List<SqlRow> weblist = createSqlQuery.findList();
			System.out.println("join query executing done!");
			
			System.out.println("weblist size :  "+weblist.size());
			System.out.println("Deleting temp table data....");
			Ebean.deleteAllPermanent(webListTemp);
			System.out.println("Deleting temp table data Done!");
			
			if(weblist !=null && weblist.size()>0) {
			return weblist;
			}
			else
			{
				return null;
			}
		}
		return null;
	}

	@Override
	public List<Website> getWebsiteList() {
		return Ebean.find(Website.class).findList();
	}

	private boolean isWebsiteUrlPresent(String Url) {
		Website website = getWebsiteByUrl(Url);
		if (website != null)
			return true;
		return false;
	}

	@Override
	public Website getWebsiteById(Long id) {
		if (id != null) {
			Website website = Ebean.find(Website.class).where().eq("id", id).findOne();
			if (website != null) {
				return website;
			}
		}
		return null;
	}

	@Override
	public boolean updateWebsite(String id, Website latestWebsite) {
		if (id != null && id.trim().length() > 0) {
			Website oldWebsite = getWebsiteById(Long.parseLong(id));
			if (oldWebsite != null) {
				oldWebsite.setPhone(latestWebsite.getPhone());
				oldWebsite.setEmail(latestWebsite.getEmail());
				oldWebsite.update();
				return true;
			}
			return false;
		}
		return false;
	}
}
