/**
 * 
 */
package com.rage.models.website.repository;

import java.util.List;

import com.google.inject.ImplementedBy;
import com.rage.models.csv.Csv;
import com.rage.models.website.Website;
import com.rage.models.website.repository.impl.WebsiteRepositoryImpl;

import io.ebean.SqlRow;

/**
 * @author neethithevan.r
 *
 */
@ImplementedBy(WebsiteRepositoryImpl.class)
public interface WebsiteRepository {
	
	public Website getWebsiteByUrl(String websiteUrl);
	
	public List<SqlRow> addWebsiteUrl(List<Website> websiteList,Csv csvDetails);
	
	public List<Website> getWebsiteList();
	
	public Website getWebsiteById(Long id);

	boolean updateWebsite(String id, Website latestWebsite);
	
}
