/**
 * 
 */
package com.rage.models.website.service;

import java.util.List;

import com.google.inject.ImplementedBy;
import com.rage.models.csv.Csv;
import com.rage.models.website.Website;
import com.rage.models.website.service.impl.WebsiteServiceImpl;

import io.ebean.SqlRow;

/**
 * @author neethithevan.r
 *
 */
@ImplementedBy(WebsiteServiceImpl.class)
public interface WebsiteService {

	public List<SqlRow>  addWebsiteUrl(List<Website> websiteList, Csv csvDetails);
	
	public Website getWebsiteByUrl(String url);
	
	public List<Website> getWebsiteList();
	
	public Website getWebsiteById(Long id);
	
	public boolean updateWebsite(String id,Website latestWebsite);
}
