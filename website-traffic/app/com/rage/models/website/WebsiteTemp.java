/**
 * 
 */
package com.rage.models.website;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.rage.models.csv.Csv;

import io.ebean.Model;

/**
 * @author neethithevan.r
 *
 */
@Entity
@Table(name = "website_temp")
public class WebsiteTemp extends Model implements Serializable {

	
	
	public WebsiteTemp(Website website,Csv csvDetails) {
		this.id=website.getId();
		this.url=website.getUrl();
		this.alexaRanking=website.getAlexaRanking();
		this.countryIp=website.getCountryIp();
		this.countryCodePage=website.getCountryCodePage();
		this.ip=website.getIp();
		this.webServer=website.getWebServer();
		this.phone=website.getPhone();
		this.email=website.getEmail();
		this.csvId=csvDetails.getId();
	}

	private static final long serialVersionUID = 8259303456445993351L;

	@Id
	private Long id;

	@Column(name = "csv_id")
	private Long csvId;
	
	@Column(name = "url")
	private String url;

	@Column(name = "alexa_ranking")
	private String alexaRanking;

	@Column(name = "country_ip")
	private String countryIp;

	@Column(name = "country_code_page")
	private String countryCodePage;

	@Column(name = "ip")
	private String ip;

	@Column(name = "web_server")
	private String webServer;

	@Column(name = "phone")
	private String phone;

	@Column(name = "email")
	private String email;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getAlexaRanking() {
		return alexaRanking;
	}

	public void setAlexaRanking(String alexaRanking) {
		this.alexaRanking = alexaRanking;
	}

	public String getCountryIp() {
		return countryIp;
	}

	public void setCountryIp(String countryIp) {
		this.countryIp = countryIp;
	}

	public String getCountryCodePage() {
		return countryCodePage;
	}

	public void setCountryCodePage(String countryCodePage) {
		this.countryCodePage = countryCodePage;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getWebServer() {
		return webServer;
	}

	public void setWebServer(String webServer) {
		this.webServer = webServer;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	

	public Long getCsvId() {
		return csvId;
	}

	public void setCsvId(Long csvId) {
		this.csvId = csvId;
	}

	

	@Override
	public String toString() {
		return "Website [id=" + id + ", url=" + url + ", alexaRanking=" + alexaRanking + ", countryIp=" + countryIp
				+ ", countryCodePage=" + countryCodePage + ", ip=" + ip + ", webServer=" + webServer + ", phone="
				+ phone + ", email=" + email + "]";
	}

}
