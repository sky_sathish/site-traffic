/**
 * 
 */
package com.rage.models.report.repository.impl;

import java.util.List;

import com.rage.models.report.main.MainReport;
import com.rage.models.report.repository.MainReportRepository;

import io.ebean.Ebean;

/**
 * @author neethithevan.r
 *
 */
public class MainReportRepositoryImpl implements MainReportRepository {

	@Override
	public boolean addMainReport(MainReport mainReport) {
		if (mainReport != null) {
			mainReport.save();
			mainReport.getProviderReport().forEach(providerReport -> {
				providerReport.setMainReportId(mainReport.getId());
			});
			
			Ebean.saveAll(mainReport.getProviderReport());
			return true;
		}
		return false;
	}

	@Override
	public List<MainReport> getMainReport(String csvId,Integer fromIndex,Integer toIndex) {
		List<MainReport> mainReportList = Ebean.find(MainReport.class)				
				.fetch("website").fetch("providerReport").where()
			
				.eq("csvId", csvId).setFirstRow(fromIndex).setMaxRows(toIndex).findList();
		if (mainReportList != null) {
		
			return mainReportList;
		}
		return null;
	}
	
	@Override
	public int getMainReportTotalcount(String csvId) {
		int mainReporttotalcount = 0;
		try {
			mainReporttotalcount = Ebean.find(MainReport.class)				
					.fetch("website").fetch("providerReport")
					.where()
					.eq("csvId", csvId).findCount();
		} catch (Exception e) {
		}
		return mainReporttotalcount;
	}

}
