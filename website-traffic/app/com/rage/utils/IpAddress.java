/**
 * 
 */
package com.rage.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import play.Logger;

/**
 * @author k.sathish
 *
 */
public class IpAddress {

	public static void main(String args[]) {
		
		
	}
	
	public String getIpAddress() {
		
		String ip="";
		
		WebDriver driver = new HtmlUnitDriver();
		try {
		driver.get("https://api.ipify.org/");
		ip=driver.getPageSource().toString().trim();
		System.out.println("----- Proxy IP: "+ip);
		Logger.info("----- Proxy IP: "+ip);
		}catch(Exception e) {
			Logger.error("getIpAddress() ->\n"+e.getMessage());
			e.printStackTrace();
		}
		
		try {
			driver.quit();	
		}catch(Exception e) {}
		return ip;
	}
	
}
