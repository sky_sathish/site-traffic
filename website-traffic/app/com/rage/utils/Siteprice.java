package com.rage.utils;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import play.Logger;

public class Siteprice {

	public List<String> getTrafficCount(String siteName,InetAddress inetAddress) {

		List<String> result=new ArrayList<>();
		siteName = siteName.replaceAll("http://", "");
		siteName = siteName.replaceAll("https://", "");
		siteName = siteName.replaceAll("www.", "");
		String link = "http://www.siteprice.org/website-worth/" + siteName;
		String DailyUniqueVisitors = "";
		String DailyPageviews = "";
		String sitePriceWebsiteReqIP ="";
		try {
			sitePriceWebsiteReqIP = inetAddress.getHostAddress();
			Document doc = Jsoup.connect(link).get();
			DailyUniqueVisitors = doc.select("#lblDailyUniqueVisitors").text();
			DailyPageviews = doc.select("#lblDailyPageviews").text();
		} catch (Exception e) {
			Logger.error("siteprice() ->\n"+e.getMessage());
		}
		
		// get current proxy ip
		IpAddress ip=new IpAddress();
		String Proxy=ip.getIpAddress();
		if(!Proxy.equals(""))
			sitePriceWebsiteReqIP=Proxy;
		
		result.add(DailyUniqueVisitors);
		result.add(sitePriceWebsiteReqIP);
		System.out.println("(f1) siteprice done!");
		return result;
	}

}
