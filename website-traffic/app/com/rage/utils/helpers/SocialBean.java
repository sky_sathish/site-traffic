package com.rage.utils.helpers;

public class SocialBean {

	
	String facebookNameBean,twitterNameBean,contactUsBean;

	public String getFacebookNameBean() {
		return facebookNameBean;
	}

	public void setFacebookNameBean(String facebookNameBean) {
		this.facebookNameBean = facebookNameBean;
	}

	public String getTwitterNameBean() {
		return twitterNameBean;
	}

	public void setTwitterNameBean(String twitterNameBean) {
		this.twitterNameBean = twitterNameBean;
	}

	public String getContactUsBean() {
		return contactUsBean;
	}

	public void setContactUsBean(String contactUsBean) {
		this.contactUsBean = contactUsBean;
	}

	
	
}
