package com.rage.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.rage.utils.helpers.StaticClass;

import play.Logger;

public class GoogleSocialMedia {


	public  String Google(String siteName, String socialMediaName) {

		siteName = siteName.replaceAll("https://", "");
		siteName = siteName.replaceAll("http://", "");
		siteName = siteName.replaceAll("www.", "");
		siteName = siteName.replaceAll(".com", "");

		String link = "";
		if (socialMediaName.equalsIgnoreCase(StaticClass.FB))
			link = "https://www.google.co.in/search?q=" + siteName.trim() + "+fb";

		if (socialMediaName.equalsIgnoreCase(StaticClass.Twitter))
			link = "https://www.google.co.in/search?q=" + siteName.trim() + "+twitter";

		String Media_Link = "";

		if (!link.equals("")) {
			try {
				Document doc = Jsoup.connect(link).get();
				Media_Link = doc.select(".g").first().select(".s").select("cite").text().trim();
				
				if (!Media_Link.equals("")) {
					String sp[] = Media_Link.split("/");
					Media_Link = sp[sp.length - 1];
				}
				
			} catch (Exception e) {Logger.error("Google() ->\n"+e.getMessage());
			}
		}
		Media_Link=Media_Link.replaceAll("\\s++", "");
		
		String MediaCount="";
		if(!Media_Link.equals("")) {
			
			if (socialMediaName.equalsIgnoreCase(StaticClass.FB))
				MediaCount=FBData.getFBFollowersCount(Media_Link);
				
			if (socialMediaName.equalsIgnoreCase(StaticClass.Twitter))
				MediaCount=TwitterData.getTwitterFollwersCount(Media_Link);
		}
		System.out.println("(f7/f8) Fb check in Google done!");
		return MediaCount;
	}
}
