package com.rage.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import com.rage.utils.helpers.SocialBean;

import play.Logger;

public class FBLink {


	public  List<SocialBean> getFaceBook_TwitterName(String siteName) {
		String FBName = "";
		String TwitterName = "";
		String contact_us_link="";
		String source="";
		
		try {
			Document doc = Jsoup.connect(siteName).get();
			Elements els = doc.select("a");
			for (Element ele : els) {
				if (ele.attr("href").contains("www.facebook.com")) {
					FBName = ele.attr("href").trim();
				}

				if (ele.attr("href").contains("www.twitter.com"))
					TwitterName = ele.attr("href").trim();
			}
			Elements a_els=doc.select("a");
			for(Element a_ele:a_els) {
				String a_tag=a_ele.text();
				if(!a_tag.isEmpty() && !a_tag.trim().equals("")) {
					
					if(a_tag.toLowerCase().contains("contact")) {
						String contact_us=a_ele.attr("href");
						if(contact_us.trim().startsWith("http") || 
							contact_us.trim().startsWith("/")) {
							contact_us_link = contact_us;
							contact_us_link = contact_us_link.replaceAll(siteName, "");
						}
					}
				}
			}
			if(!contact_us_link.startsWith("http") && !contact_us_link.trim().equals("")) {
			if(siteName.endsWith("/") && contact_us_link.startsWith("/")) {
				contact_us_link=siteName+contact_us_link.substring(1, contact_us_link.length());
			}
			else if(!siteName.endsWith("/") && contact_us_link.startsWith("/")) {
				contact_us_link=siteName+contact_us_link;
			}
			else if(siteName.endsWith("/") && !contact_us_link.startsWith("/")) {
				contact_us_link=siteName+contact_us_link;
			}
			else if(!siteName.endsWith("/") && !contact_us_link.startsWith("/")) {
				contact_us_link=siteName+"/"+contact_us_link;
			}
			}
		} catch (Exception e) {
			Logger.error("FBLing() ->\n"+e.getMessage());
			e.printStackTrace();
		}

		try {
			
		
		// Clean FB Link
		if (!FBName.equals("")) {
			String sp[] = FBName.split("/");
			FBName = sp[sp.length - 1];
		}

		// Clean Twitter Link
		if (!TwitterName.equals("")) {
			String sp[] = TwitterName.split("/");
			TwitterName = sp[sp.length - 1];
		}
		}catch (Exception e1) {}
		
		List<SocialBean> bean_list = new ArrayList<>();
		SocialBean bean = new SocialBean();
		bean.setFacebookNameBean(FBName);
		bean.setTwitterNameBean(TwitterName);
		bean.setContactUsBean(contact_us_link);
		bean_list.add(bean);
//		System.out.println("FBName : " + FBName);
//		System.out.println("TwitterName : " + TwitterName);
//		System.out.println("contact_us_link : " + contact_us_link);
		
		System.out.println("(f6) get FB, Twiter, Contact Done!");
		return bean_list;
	}

	public static String getDriverSourceCode(WebDriver driver,String siteName) {
		String source="";
		try {
			driver.get(siteName);
			source=driver.getPageSource();
		}catch(Exception e) {}
		
		return source;
	}
}
