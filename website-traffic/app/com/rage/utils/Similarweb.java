package com.rage.utils;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import play.Logger;

public class Similarweb {

	public  List<String> getTrafficCount(String siteName,InetAddress inetAddress) {
		List<String> result=new ArrayList<>();
		siteName = siteName.replaceAll("https://", "");
		siteName = siteName.replaceAll("http://", "");
		siteName = siteName.replaceAll("www.", "");
		String similarWebsiteReqIP ="";
		String link = "https://www.similarweb.com/website/" + siteName + "#overview";
		String Total_Visits = "";
		String Avg_Visit_Duration = "";
		String Pages_per_Visit = "";
		String Bounce_Rate = "";

		try {
			similarWebsiteReqIP=inetAddress.getHostAddress();
			Document doc = Jsoup.connect(link).get();
			Elements els = doc.select(".engagementInfo-line").select(".engagementInfo-content");
			for (Element ele : els) {
				String title = ele.select(".engagementInfo-param").text();
				String value = ele.select(".engagementInfo-valueNumber").text();
				if (title.trim().equals("Total Visits"))
					Total_Visits = value;

				if (title.trim().equals("Avg. Visit Duration"))
					Avg_Visit_Duration = value;

				if (title.trim().equals("Pages per Visit"))
					Pages_per_Visit = value;

				if (title.trim().equals("Bounce Rate"))
					Bounce_Rate = value;
			}
		} catch (Exception e) {Logger.error("similarweb() ->\n"+e.getMessage());}
		
		// get current proxy ip
		IpAddress ip=new IpAddress();
		String Proxy=ip.getIpAddress();
		if(!Proxy.equals(""))
			similarWebsiteReqIP=Proxy;
				
		result.add(Total_Visits);
		result.add(similarWebsiteReqIP);
		System.out.println("(f2) similarweb done!");
		return result;
	}
}
