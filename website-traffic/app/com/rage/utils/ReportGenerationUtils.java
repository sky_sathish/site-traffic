package com.rage.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.util.CellRangeAddress;

import com.google.inject.Inject;
import com.rage.models.report.main.MainReport;
import com.rage.models.report.provider.ProviderReport;
import com.rage.models.report.service.MainReportService;
import com.rage.models.website.Website;
import com.rage.models.website.service.WebsiteService;

public class ReportGenerationUtils {

	private static final int REPORT_SHOW_RECORD_COUNT = 10000;
	
	@Inject
	public  MainReportService mainReportServ;
	
	@Inject
	public  WebsiteService websiteServ;

	public  File createExcelFile(String csvId) {

		
		File excelFile = null;
		try {
			String reportName = "Report";
			Date date = new Date();
			String fileName = reportName.replace(" ", "_") + "_" + date.getTime() + ".xls";
			String file = fileName;
			excelFile = new File(file);
			excelFile.setReadable(true);
			excelFile.setWritable(true);
			excelFile.setExecutable(true);
			
			// Create workbook
			HSSFWorkbook wb = new HSSFWorkbook();
			// creation helper
			CreationHelper creationHelper = wb.getCreationHelper();

			// sathish changes
			int totalRecodCount = mainReportServ.getMainReportTotalcount(csvId);
			int sheetCount = totalRecodCount/REPORT_SHOW_RECORD_COUNT;
			if(totalRecodCount < REPORT_SHOW_RECORD_COUNT) {
				sheetCount=1;
			}
			System.out.println("totalRecodCount : "+totalRecodCount);
			System.out.println("sheetCount : "+sheetCount);
			for(int i=1;i<=sheetCount;i++) {
				List<MainReport> mainReportList = new ArrayList<>();
				int fromIndex = ((i - 1) * REPORT_SHOW_RECORD_COUNT);
				int toIndex=REPORT_SHOW_RECORD_COUNT;
				List<MainReport> mainReportLst = mainReportServ.getMainReport(csvId,fromIndex,toIndex);
				mainReportLst.forEach(mainReport -> {
					if (mainReport != null && mainReport.getSiteId() != null) {
						Website website = websiteServ.getWebsiteById(mainReport.getSiteId());
						mainReport.setWebsite(website);
					}
				});
				mainReportList.addAll(mainReportLst);
			
			int rowCount = 0;
			// Sheet creation
			String sheetName = "Report-"+i;
			Sheet sheet = wb.createSheet(sheetName);

			// Create a Font for styling header cells
			Font headerFont = wb.createFont();
			headerFont.setBold(true);
			headerFont.setFontHeightInPoints((short) 10);
			headerFont.setColor(IndexedColors.BLACK.index);

			// Create a CellStyle with the font
			CellStyle headerCellStyle = wb.createCellStyle();
			headerCellStyle.setFillBackgroundColor(IndexedColors.BLUE.index);
			headerCellStyle.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.index);
			headerCellStyle.setFont(headerFont);

//			createHeader(mainReportList, sheet, headerCellStyle, rowCount, wb);
			createHeaderNew(mainReportList, sheet, headerCellStyle, rowCount, wb);
			
			// Create Cell Style for formatting Date
			CellStyle dateCellStyle = wb.createCellStyle();
			dateCellStyle.setDataFormat(creationHelper.createDataFormat().getFormat("dd-MM-yyyy"));

			// sheet.autoSizeColumn(0);

			FileOutputStream fileOut;
			fileOut = new FileOutputStream(excelFile);
			wb.write(fileOut);
			fileOut.close();
               		 excelFile.setReadable(true);
                        excelFile.setWritable(true);
                        excelFile.setExecutable(true);
			wb.close();
			
			System.out.println(((i*100)/sheetCount)+"% Done!");
			}// for end
			
			

		} catch (IOException e) {
			System.out.println("Exception ::: " + e.getMessage());
			e.printStackTrace();
		}
		return excelFile;
	}

	private static void createHeaderNew(List<MainReport> mainReportList, Sheet sheet, CellStyle headerCellStyle,
			int rowCount, HSSFWorkbook wb) {
		// Create a Row
		int rowNumStart = 0, rowNumEnd = 1, colNumStart = 0, colNumEnd = 0, cellNum = 0;
//		System.out.println(rowCount);
		Row headerRow = sheet.createRow(rowCount);
		rowCount += 1;
		Row row = sheet.createRow(rowCount);
		if (mainReportList != null && !mainReportList.isEmpty()) {
			MainReport mainReport = mainReportList.get(0);
			if (mainReport.getSiteUrl() != null) {
				mergeRowHeader(sheet, headerCellStyle, rowNumStart, rowNumEnd, colNumStart, colNumEnd, cellNum,
						"SITE URL", headerRow);
			}
//			if (mainReport.getWebsite() != null && mainReport.getWebsite().getAlexaRanking() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				mergeRowHeader(sheet, headerCellStyle, rowNumStart, rowNumEnd, colNumStart, colNumEnd, cellNum,
						"ALEXA RANKING", headerRow);
//			}
//			if (mainReport.getMonthlyUniqueVisitors() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "MONTHLY UNIQUE VISITORS", cellNum);
//			}
//			if (mainReport.getMonthlyRevenue() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "MONTHLY REVENUE", cellNum);
//			}

//			if (mainReport.getYearlyUniqueVisitors() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "YEARLY UNIQUE VISITORS", cellNum);
//			}
//			if (mainReport.getYearlyRevenue() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "YEARLY REVENUE", cellNum);
//			}
//			if (mainReport.getWebsite() != null && mainReport.getWebsite().getEmail() != null) {
					colNumStart += 1;
					colNumEnd += 1;
					cellNum += 1;
					mergeRowHeader(sheet, headerCellStyle, rowNumStart, rowNumEnd, colNumStart, colNumEnd, cellNum,
							"EMAIL ID", headerRow);
//			}
//			if (mainReport.getWebsite() != null && mainReport.getWebsite().getEmail() != null) {
							colNumStart += 1;
							colNumEnd += 1;
							cellNum += 1;
							mergeRowHeader(sheet, headerCellStyle, rowNumStart, rowNumEnd, colNumStart, colNumEnd, cellNum,
									"COUNTRY IP", headerRow);
//			}			
				
			CellStyle style = setFont(wb);
			rowCount += 1;
			setReport(mainReportList, sheet, style, rowCount);
		}
	}

	private static void createHeader(List<MainReport> mainReportList, Sheet sheet, CellStyle headerCellStyle,
			int rowCount, HSSFWorkbook wb) {
		// Create a Row
		int rowNumStart = 0, rowNumEnd = 1, colNumStart = 0, colNumEnd = 0, cellNum = 0;
//		System.out.println(rowCount);
		Row headerRow = sheet.createRow(rowCount);
		rowCount += 1;
		Row row = sheet.createRow(rowCount);
		if (mainReportList != null && !mainReportList.isEmpty()) {
			MainReport mainReport = mainReportList.get(0);
			if (mainReport.getSiteUrl() != null) {
				mergeRowHeader(sheet, headerCellStyle, rowNumStart, rowNumEnd, colNumStart, colNumEnd, cellNum,
						"SITE URL", headerRow);
			}
//			if (mainReport.getWebsite() != null && mainReport.getWebsite().getAlexaRanking() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				mergeRowHeader(sheet, headerCellStyle, rowNumStart, rowNumEnd, colNumStart, colNumEnd, cellNum,
						"ALEXA RANKING", headerRow);
//			}
//			if (mainReport.getWebsite() != null && mainReport.getWebsite().getEmail() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				mergeRowHeader(sheet, headerCellStyle, rowNumStart, rowNumEnd, colNumStart, colNumEnd, cellNum,
						"EMAIL ID", headerRow);
//			}
//			if (mainReport.getWebsite() != null && mainReport.getWebsite().getPhone() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				mergeRowHeader(sheet, headerCellStyle, rowNumStart, rowNumEnd, colNumStart, colNumEnd, cellNum, "PHONE",
						headerRow);
//			}
//			if (mainReport.getWebsite() != null && mainReport.getEmailInfo() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				mergeRowHeader(sheet, headerCellStyle, rowNumStart, rowNumEnd, colNumStart, colNumEnd, cellNum, "EXTRACTED EMAIL",
						headerRow);
//			}
//			if (mainReport.getWebsite() != null && mainReport.getPhoneInfo() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				mergeRowHeader(sheet, headerCellStyle, rowNumStart, rowNumEnd, colNumStart, colNumEnd, cellNum, "EXTRACTED PHONE",
						headerRow);
//			}
//			if (mainReport.getFbFollowers() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				mergeRowHeader(sheet, headerCellStyle, rowNumStart, rowNumEnd, colNumStart, colNumEnd, cellNum,
						"FACEBOOK FOLLOWERS", headerRow);
//			}
//			if (mainReport.getTwitterFollowers() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				mergeRowHeader(sheet, headerCellStyle, rowNumStart, rowNumEnd, colNumStart, colNumEnd, cellNum,
						"TWITTER FOLLOWERS", headerRow);
//			}
//			if (mainReport.getFbUrl() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				mergeRowHeader(sheet, headerCellStyle, rowNumStart, rowNumEnd, colNumStart, colNumEnd, cellNum,
						"FACEBOOK URL", headerRow);
//			}
//			if (mainReport.getTwitterUrl() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				mergeRowHeader(sheet, headerCellStyle, rowNumStart, rowNumEnd, colNumStart, colNumEnd, cellNum,
						"TWITTER URL", headerRow);
//			}
			if (mainReport.getProviderReport() != null && !mainReport.getProviderReport().isEmpty()) {
				List<ProviderReport> providerReport = mainReport.getProviderReport();
				if (providerReport != null) {
					int websiteSize = providerReport.size();
					int reportRowStart = 0, reportRowEnd = 0;
					colNumStart += 1;
					cellNum += 1;
					colNumEnd += websiteSize;
					mergeRowHeader(sheet, headerCellStyle, reportRowStart, reportRowEnd, colNumStart, colNumEnd,
							cellNum++, "TRAFFIC", headerRow);
					cellNum -= 1;
					for (ProviderReport provReport : providerReport) {
						if (provReport != null && provReport.getSiteName() != null
								&& !provReport.getSiteName().isEmpty()) {
							rowHeader(sheet, headerCellStyle, rowCount, cellNum, provReport.getSiteName().toUpperCase(),
									row);
							cellNum += 1;
						}
					}
				}
			}
//			if (mainReport.getDailyUniqueVisitors() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				setExcelValue(sheet, row, headerCellStyle, "DAILY UNIQUE VISITORS", cellNum);
//			}
//			if (mainReport.getDailyRevenue() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "DAILY REVENUE", cellNum);
//			}
//			if (mainReport.getDailyUniquePageviews() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "DAILY UNIQUE PAGE VIEWS", cellNum);
//			}
//			if (mainReport.getMonthlyUniqueVisitors() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "MONTHLY UNIQUE VISITORS", cellNum);
//			}
//			if (mainReport.getMonthlyRevenue() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "MONTHLY REVENUE", cellNum);
//			}
//			if (mainReport.getMonthlyUniquePageviews() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "MONTHLY UNIQUE PAGE VIEWS", cellNum);
//			}
//			if (mainReport.getYearlyUniqueVisitors() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "YEARLY UNIQUE VISITORS", cellNum);
//			}
//			if (mainReport.getYearlyRevenue() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "YEARLY REVENUE", cellNum);
//			}
//			if (mainReport.getYearlyUniquePageviews() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "YEARLY UNIQUE PAGE VIEWS", cellNum);
//			}
//			if (mainReport.getGlobalRank() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "GLOBAL RANK", cellNum);
//			}
//			if (mainReport.getCountryRank() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "COUNTRY RANK", cellNum);
//			}
//			if (mainReport.getCountryLabel() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "COUNTRY LABEL", cellNum);
//			}
//			if (mainReport.getWebsiteLoadTime() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "WEBSITE LOAD TIME", cellNum);
//			}
//			if (mainReport.getWebsitePageSize() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "WEBSITE PAGES SIZE", cellNum);
//			}
//			if (mainReport.getDomainName() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "DOMAIN NAME", cellNum);
//			}
//			if (mainReport.getDomainOrganization() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "DOMAIN ORGANIZATION", cellNum);
//			}
//			if (mainReport.getDomainFax() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "DOMAIN FAX", cellNum);
//			}
//			if (mainReport.getDomainPhone() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "DOMAIN PHONE", cellNum);
//			}
//			if (mainReport.getDomainRegistrationDate() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "DOMAIN REGISTRATION DATE", cellNum);
//			}
//			if (mainReport.getDomainLastUpdated() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "DOMAIN LAST UPDATED", cellNum);
//			}
//			if (mainReport.getDomainExpirationDate() != null) {
				colNumStart += 1;
				colNumEnd += 1;
				cellNum += 1;
				setExcelValue(sheet, row, headerCellStyle, "DOMAIN EXPIRATION DATE", cellNum);
//			}
			CellStyle style = setFont(wb);
			rowCount += 1;
			setReport(mainReportList, sheet, style, rowCount);
		}
	}
	private static CellStyle setFont(HSSFWorkbook wb) {
		Font font = wb.createFont();
		font.setFontHeightInPoints((short) 10);
		font.setColor(IndexedColors.BLACK.getIndex());
		CellStyle headerCellStyle = wb.createCellStyle();
		headerCellStyle.setFont(font);
		return headerCellStyle;
	}

	private static void mergeRowHeader(Sheet sheet, CellStyle headerCellStyle, int rowNumStart, int rowNumEnd,
			int colNumStart, int colNumEnd, int cellNum, String headerName, Row headerRow) {
		Cell cell = headerRow.createCell(cellNum);
		cell.setCellValue(headerName);
		headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		cell.setCellStyle(headerCellStyle);
		sheet.addMergedRegion(new CellRangeAddress(rowNumStart, rowNumEnd, colNumStart, colNumEnd));
	}

	private static void rowHeader(Sheet sheet, CellStyle headerCellStyle, int rowCount, int cellNum, String headerName,
			Row row) {
		Cell cell = row.createCell(cellNum);
		cell.setCellValue(headerName);
		headerCellStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerCellStyle.setFillForegroundColor(HSSFColor.BRIGHT_GREEN.index);
		cell.setCellStyle(headerCellStyle);
	}

	private static void setReport(List<MainReport> mainReportList, Sheet sheet, CellStyle style, int rowCount) {

		for (MainReport mainReport : mainReportList) {
			Row row = sheet.createRow(rowCount);
//			setReportValue(mainReport, sheet, style, row);
			setReportValueNew(mainReport, sheet, style, row);
			rowCount += 1;
		}

	}

	private static void setReportValue(MainReport mainReport, Sheet sheet, CellStyle style, Row row) {
		int cellNum = 0;
		if (mainReport != null) {
			if (mainReport.getSiteUrl() != null) {
				setExcelValue(sheet, row, style, mainReport.getSiteUrl(), cellNum);
			}else {
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getWebsite() != null && mainReport.getWebsite().getAlexaRanking() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getWebsite().getAlexaRanking(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getWebsite() != null && mainReport.getWebsite().getEmail() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getWebsite().getEmail(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getWebsite() != null && mainReport.getWebsite().getPhone() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getWebsite().getPhone(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getWebsite() != null && mainReport.getEmailInfo() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getEmailInfo(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getWebsite() != null && mainReport.getPhoneInfo() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getPhoneInfo(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getFbFollowers() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getFbFollowers(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getTwitterFollowers() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getTwitterFollowers(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getFbUrl() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getFbUrl(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getTwitterUrl() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getTwitterUrl(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getProviderReport() != null) {
				List<ProviderReport> providerReport = mainReport.getProviderReport();
				if (providerReport != null && !providerReport.isEmpty()) {
					for (ProviderReport provReport : providerReport) {
						if (provReport.getTrafficCount() != null) {
							cellNum += 1;
							setExcelValue(sheet, row, style, provReport.getTrafficCount(), cellNum);
						}else {
							cellNum += 1;
							setExcelValue(sheet, row, style, "", cellNum);
						}
					}
				}
			}
			if (mainReport.getDailyUniqueVisitors() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getDailyUniqueVisitors(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getDailyRevenue() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getDailyRevenue(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getDailyUniquePageviews() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getDailyRevenue(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getMonthlyUniqueVisitors() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getMonthlyUniqueVisitors(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getMonthlyRevenue() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getMonthlyRevenue(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getMonthlyUniquePageviews() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getDailyRevenue(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getYearlyUniqueVisitors() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getYearlyUniqueVisitors(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getYearlyRevenue() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getYearlyRevenue(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getYearlyUniquePageviews() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getYearlyUniquePageviews(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getGlobalRank() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getGlobalRank(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getCountryRank() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getCountryRank(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getCountryLabel() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getCountryLabel(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getWebsiteLoadTime() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getWebsiteLoadTime(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getWebsitePageSize() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getWebsitePageSize(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getDomainName() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getDomainName(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getDomainOrganization() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getDomainOrganization(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getDomainFax() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getDomainFax(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getDomainPhone() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getDomainPhone(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getDomainRegistrationDate() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getDomainRegistrationDate(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getDomainLastUpdated() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getDomainLastUpdated(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getDomainExpirationDate() != null) {
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getDomainExpirationDate(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
		}
	}

	private static void setReportValueNew(MainReport mainReport, Sheet sheet, CellStyle style, Row row) {
		int cellNum = 0;
		if (mainReport != null) {
			if (mainReport.getSiteUrl() != null) {// SiteUrl
				setExcelValue(sheet, row, style, mainReport.getSiteUrl(), cellNum);
			}else {
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getWebsite() != null && mainReport.getWebsite().getAlexaRanking() != null) {//AlexaRanking
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getWebsite().getAlexaRanking(), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getMonthlyUniqueVisitors() != null && mainReport.getMonthlyUniqueVisitors().trim() != "" 
					&& mainReport.getMonthlyUniqueVisitors().trim() != "0") {// MonthlyUniqueVisitors
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getMonthlyUniqueVisitors().trim().replaceAll(",", "").replaceAll(" +", " "), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getMonthlyRevenue() != null && mainReport.getMonthlyRevenue().trim() != ""
					&& mainReport.getMonthlyRevenue().trim() != "0") {// MonthlyRevenue
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getMonthlyRevenue().replaceAll(" +", " "), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getYearlyUniqueVisitors() != null && mainReport.getYearlyUniqueVisitors().trim() !=""
					&& mainReport.getYearlyUniqueVisitors().trim() !="0") {// YearlyUniqueVisitors
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getYearlyUniqueVisitors().trim().replaceAll(",", "").replaceAll(" +", " "), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getYearlyRevenue() != null && mainReport.getYearlyRevenue().trim() != ""
					&& mainReport.getYearlyRevenue().trim() != "0") {// YearlyRevenue
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getYearlyRevenue().replaceAll(" +", " "), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getWebsite() != null && mainReport.getWebsite().getEmail() != null 
					&& mainReport.getWebsite().getEmail().trim() != "") {// Email
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getWebsite().getEmail().trim().replaceAll(",", ";").replaceAll(" +", " "), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}
			if (mainReport.getWebsite() != null && mainReport.getWebsite().getCountryIp() != null 
					&& mainReport.getWebsite().getCountryIp().trim() != "") {// CountryIp
				cellNum += 1;
				setExcelValue(sheet, row, style, mainReport.getWebsite().getCountryIp().trim().replaceAll(",", ";").replaceAll(" +", " "), cellNum);
			}else {
				cellNum += 1;
				setExcelValue(sheet, row, style, "", cellNum);
			}

		}
	}

	
	private static void setExcelValue(Sheet sheet, Row row, CellStyle style, String value, int cellNum) {
		Cell cell = row.createCell(cellNum);
		cell.setCellValue(value);
		style.setAlignment(CellStyle.ALIGN_CENTER);
		cell.setCellStyle(style);
	}
}
