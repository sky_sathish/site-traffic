package com.rage.utils;

import java.net.InetAddress;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.rage.models.report.main.MainReport;
import com.rage.models.report.provider.ProviderReport;
import com.rage.models.website.Website;
import com.rage.utils.helpers.SocialBean;
import com.rage.utils.helpers.StaticClass;

import play.Logger;
import play.libs.Json;

public class RootClass {

	@SuppressWarnings("unchecked")
	public static MainReport getData(Long webId,String webUrl) {
		
		System.setProperty("http.proxyHost", "127.0.0.1");
		System.setProperty("http.proxyPort", "8118");
		int siteDoneCount=0;
			siteDoneCount++;
			MainReport mainReport = new MainReport();
			mainReport.setSiteId(webId);
			String siteName = webUrl;
			
			siteName = siteName.trim();
			if (!siteName.startsWith("http"))
				siteName = "http://" + siteName;
			if (siteName.endsWith("/"))
				siteName = siteName.substring(0, siteName.length() - 1);
			String Siteprice_visit_count = "";
			String Similarweb_visit_count = "";
			String Siteworthtraffic_visit_count = "";
			String Mysitewealth_visit_count = "";
			String Statchest_visit_count = "";
			String FBFollowers = "";
			String TwitterFollowers = "";
			String sitePriceWebsiteReqIP = "";
			String similarWebsiteReqIP = "";
			String siteWorthWebsiteReqIP = "";
			String mySiteWealthWebsiteReqIP = "";
			String StatchestWebsiteReqIP = "";

			List<Set<String>> phone_email_info=new ArrayList<>();
			
			ObjectNode Siteworthtraffic_objnode = Json.newObject();
			ObjectNode Mysitewealth_objnode = Json.newObject();

			ContactInfo contactInfo=new ContactInfo();
			// MultiThread Started
			ExecutorService executor = Executors.newWorkStealingPool(9);
			String passSite = siteName;
			Siteprice siteprice = new Siteprice();
			Similarweb similarweb = new Similarweb();
			Siteworthtraffic siteworthtraffic = new Siteworthtraffic();
			Mysitewealth mysitewealth = new Mysitewealth();
			Statchest statchest = new Statchest();
			FBLink fbLink= new FBLink();
			GoogleSocialMedia googleSocialMedia = new GoogleSocialMedia();

			String fbName = "";
			String twitterName = "";
			String contact_us_link = "";
			
			System.out.println("Start site : "+passSite);
			Logger.info("Passing site : "+passSite);
			String ErrorPagealert=null;
			
			ErrorPageChecker error=new ErrorPageChecker();
			ErrorPagealert=error.errorPageChecking(passSite);
			System.out.println("ErrorPagealert "+ErrorPagealert);

			if(ErrorPagealert !=null && !ErrorPagealert.equals("timeout")) {
				Logger.error("page error -> site name: "+passSite+" | ErrorPageAlertMsg: '"+ErrorPagealert+"'");
			}
			else {
			// get Traffic visited count ----------------------------------
				
				String pagestatus="";
				if(ErrorPagealert==null) {
					pagestatus="";}
				else {
					pagestatus=ErrorPagealert;
				}
			
			
			try {
			InetAddress inetAddress = InetAddress.getLocalHost();
			// site 1
			List<String> sitePriceResult=new ArrayList<>();
			Future<?> f1 = executor.submit(() -> {
				return siteprice.getTrafficCount(passSite,inetAddress);
			});
			
			// site 2
			List<String> similarWebResult=new ArrayList<>();
			Future<?> f2 = executor.submit(() -> {
				return similarweb.getTrafficCount(passSite,inetAddress);
			});
			
			// site 3
			Future<?> f3 = executor.submit(() -> {
				return siteworthtraffic.getTrafficCountNew(passSite,inetAddress);
			});

			// site 4
			Future<?> f4 = executor.submit(() -> {
				return mysitewealth.getTrafficCountNew(passSite,inetAddress);
			});
			
			// site 5
			List<String> statChestResult=new ArrayList<>();
			Future<?> f5 = executor.submit(() -> {
				return statchest.getTrafficCount(passSite,inetAddress);
			});

			// create bean object -------------------------------------------
			List<SocialBean> bean_list = new ArrayList<>();
			// get FaceBook & Twitter Names
			String page=pagestatus;
			Future<?> f6 = executor.submit(() -> {
				
				if(!page.equals("timeout")) {
					return fbLink.getFaceBook_TwitterName(passSite);
				}
				else
					return "timeout";
			});
			String timpout1="";
			try {
				timpout1=(String) f6.get();
			}catch(Exception e) {}
			
			if(!timpout1.equals("timeout")) {
			bean_list =  (List<SocialBean>) f6.get();// Assign bean value
			if(bean_list!=null && !bean_list.isEmpty()) {
			for (SocialBean socialLink : bean_list) {
				fbName = socialLink.getFacebookNameBean();
				twitterName = socialLink.getTwitterNameBean();
				contact_us_link = socialLink.getContactUsBean();
			}
			}
			}
			// get FB name ---------------------------------------------------
			Future<?> f7 =null;
			if (fbName.equals("")) {
				f7 = executor.submit(() -> {
					return googleSocialMedia.Google(passSite, StaticClass.FB);
				});
//				FBFollowers = (String) f7.get();// Assign value
			}else {
				String fbName2=fbName;
				f7 = executor.submit(() -> {
					return FBData.getFBFollowersCount(fbName2);
				});
//				FBFollowers = (String) f7.get();// Assign value
			}

			// get Twitter name --------------------------------------------------
			Future<?> f8 = null;
			if (twitterName.equals("")) {
				f8 = executor.submit(() -> {
					return googleSocialMedia.Google(passSite, StaticClass.Twitter);
				});
//				TwitterFollowers = (String) f8.get();// Assign value
			}else {
				String twitterName2=twitterName;
				f8 = executor.submit(() -> {
					return TwitterData.getTwitterFollwersCount(twitterName2);
				});
//				TwitterFollowers = (String) f8.get();// Assign value
			}
			
			// get ContactUs info --------------------------------------------------
			String contactUsLink=contact_us_link;
			String page2=pagestatus;
			Future<?> f9 = executor.submit(() -> {
				if(!page2.equals("timeout")) {
					return contactInfo.sitesSpliter(passSite, contactUsLink);
				}
				return "timeout";
				});
		
			
			while(true) {
				
				if((f1.isDone() || f1.isCancelled()) && (f2.isDone() || f2.isCancelled()) &&
					(f3.isDone() || f3.isCancelled()) && (f4.isDone() || f4.isCancelled()) &&
					(f5.isDone() || f5.isCancelled()) && (f6.isDone() || f6.isCancelled()) &&
					(f7.isDone() || f7.isCancelled()) && (f8.isDone() || f8.isCancelled()) &&
					(f9.isDone() || f9.isCancelled())) {
					try {
					    System.out.println("attempt to shutdown executor -> Site '"+passSite+"' is Done!!!");
					    executor.shutdown();
					    executor.awaitTermination(5, TimeUnit.SECONDS);
					}
					catch (InterruptedException e) {
					    System.err.println("tasks interrupted");
					}
					break;
				}
				
			}// while end

			//site 1
			sitePriceResult = (List<String>) f1.get();// Assign value
			if(!sitePriceResult.isEmpty()) {
				Siteprice_visit_count=sitePriceResult.get(0);
				sitePriceWebsiteReqIP=sitePriceResult.get(1);
			}
			// site 2
			similarWebResult = (List<String>) f2.get();// Assign value
			if(!similarWebResult.isEmpty()) {
				Similarweb_visit_count=similarWebResult.get(0);
				similarWebsiteReqIP=similarWebResult.get(1);
			}
			// site 3
			Siteworthtraffic_objnode =  (ObjectNode) f3.get();// Assign value
			// site 4
			Mysitewealth_objnode = (ObjectNode) f4.get();// Assign value
			// site 5
			statChestResult=(List<String>) f5.get();// Assign value
			if(!statChestResult.isEmpty()) {
				Statchest_visit_count=statChestResult.get(0);
				StatchestWebsiteReqIP=statChestResult.get(1);
			}
			// get FB followers
			FBFollowers = (String) f7.get();// Assign value
			// get twitter followers
			TwitterFollowers = (String) f8.get();// Assign value
			// contact info
			String timpout2="";
			try {
				timpout2=(String) f9.get();
			}catch(Exception e) {}
			if(!timpout2.equals("timeout")) {
			phone_email_info.addAll((Collection<? extends Set<String>>) f9.get());// Assign value
			}
			}catch(Exception e) {e.printStackTrace();}
			
			
			
			
			// site url
			mainReport.setSiteUrl(siteName);

			// has fb page
			if (fbName.equals("")) {
				mainReport.setHasFbPage("false");
				FBFollowers="";
			} else {
				mainReport.setHasFbPage("true");
				mainReport.setFbUrl("https://www.facebook.com/" + fbName);
			}

			// has twitter page
			if (twitterName.equals("")) {
				mainReport.setHasTwitterPage("false");
				TwitterFollowers="";
			} else {
				mainReport.setHasTwitterPage("true");
				mainReport.setTwitterUrl("https://twitter.com/" + twitterName);
			}

			// fb followers
			FBFollowers = FBFollowers.replaceAll("[^0-9,]", "").trim();
			if (FBFollowers.equals("")) {
				FBFollowers = "0";
			}
			mainReport.setFbFollowers(FBFollowers);

			// twitter followers
			TwitterFollowers = TwitterFollowers.trim();
			if (TwitterFollowers.equals("")) {
				TwitterFollowers = "0";
			}
			mainReport.setTwitterFollowers(TwitterFollowers);

			/* sathish changed */
			
			//(ContactUs) Phone and Email information
			String phoneInfo="";
			String emailInfo="";
			
			if(!phone_email_info.isEmpty()) {
			phoneInfo=phone_email_info.get(0).toString();
			emailInfo=phone_email_info.get(1).toString();
			}
			mainReport.setPhoneInfo(phoneInfo.replaceAll("\\[", "").replaceAll("\\]", ""));
			mainReport.setEmailInfo(emailInfo.replaceAll("\\[", "").replaceAll("\\]", ""));

			// siteworthtraffic
			if (Siteworthtraffic_objnode != null) {
				if (Siteworthtraffic_objnode.hasNonNull("Daily_Unique_Visitors")) {
					mainReport.setDailyUniqueVisitors(Siteworthtraffic_objnode.get("Daily_Unique_Visitors").asText());
					Siteworthtraffic_visit_count = Siteworthtraffic_objnode.get("Daily_Unique_Visitors").asText();
				}
				if (Siteworthtraffic_objnode.hasNonNull("Daily_Revenue_(From_Ads)")) {
					mainReport.setDailyRevenue(Siteworthtraffic_objnode.get("Daily_Revenue_(From_Ads)").asText());
				}
				if (Siteworthtraffic_objnode.hasNonNull("Daily_Unique_Pageviews")) {
					mainReport.setDailyUniquePageviews(Siteworthtraffic_objnode.get("Daily_Unique_Pageviews").asText());
				}
				if (Siteworthtraffic_objnode.hasNonNull("Monthly_Unique_Visitors")) {
					mainReport
							.setMonthlyUniqueVisitors(Siteworthtraffic_objnode.get("Monthly_Unique_Visitors").asText());
				}
				if (Siteworthtraffic_objnode.hasNonNull("Monthly_Revenue_(From_Ads)")) {
					mainReport.setMonthlyRevenue(Siteworthtraffic_objnode.get("Monthly_Revenue_(From_Ads)").asText());
				}
				if (Siteworthtraffic_objnode.hasNonNull("Monthly_Unique_Pageviews")) {
					mainReport.setMonthlyUniquePageviews(
							Siteworthtraffic_objnode.get("Monthly_Unique_Pageviews").asText());
				}
				if (Siteworthtraffic_objnode.hasNonNull("Yearly_Unique_Visitors")) {
					mainReport.setYearlyUniqueVisitors(Siteworthtraffic_objnode.get("Yearly_Unique_Visitors").asText());
				}
				if (Siteworthtraffic_objnode.hasNonNull("Yearly_Revenue_(From_Ads)")) {
					mainReport.setYearlyRevenue(Siteworthtraffic_objnode.get("Yearly_Revenue_(From_Ads)").asText());
				}
				if (Siteworthtraffic_objnode.hasNonNull("Yearly_Unique_Pageviews")) {
					mainReport
							.setYearlyUniquePageviews(Siteworthtraffic_objnode.get("Yearly_Unique_Pageviews").asText());
				}
				if (Siteworthtraffic_objnode.hasNonNull("siteWorthWebsiteReqIP")) {
					siteWorthWebsiteReqIP=Siteworthtraffic_objnode.get("siteWorthWebsiteReqIP").asText();
				}
			}

			// Mysitewealth
			if (Mysitewealth_objnode != null) {

				if (Mysitewealth_objnode.hasNonNull("Global_Rank")) {
					mainReport.setGlobalRank(Mysitewealth_objnode.get("Global_Rank").asText());
				}
				if (Mysitewealth_objnode.hasNonNull("mySiteWealthWebsiteReqIP")) {
					mySiteWealthWebsiteReqIP=Mysitewealth_objnode.get("mySiteWealthWebsiteReqIP").asText();
				}

				if (Mysitewealth_objnode.hasNonNull("Rank")) {
					ArrayNode arrayNode = (ArrayNode) Mysitewealth_objnode.get("Rank");

					if (arrayNode != null) {
						for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
							JsonNode obj = it.next();
							if (obj.hasNonNull("Country_Rank")) {
								mainReport.setCountryRank(obj.get("Country_Rank").asText());
							}
							if (obj.hasNonNull("Country_Label")) {
								mainReport.setCountryLabel(obj.get("Country_Label").asText().trim());
							}
						}
					}
				}

				if (Mysitewealth_objnode.hasNonNull("Traffic_Stats")) {
					ArrayNode arrayNode = (ArrayNode) Mysitewealth_objnode.get("Traffic_Stats");

					if (arrayNode != null) {
						for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
							JsonNode obj = it.next();
							if (obj.hasNonNull("Daily_Unique_Visitors")) {
								Mysitewealth_visit_count = obj.get("Daily_Unique_Visitors").asText();
							}

						}
					}
				}

				if (Mysitewealth_objnode.hasNonNull("Website_Performance")) {
					ArrayNode arrayNode = (ArrayNode) Mysitewealth_objnode.get("Website_Performance");

					if (arrayNode != null) {
						for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
							JsonNode obj = it.next();
							if (obj.hasNonNull("Website_Load_Time")) {
								mainReport.setWebsiteLoadTime(obj.get("Website_Load_Time").asText());
							}
							if (obj.hasNonNull("Website_Page_Size")) {
								mainReport.setWebsitePageSize(obj.get("Website_Page_Size").asText().trim());
							}
						}
					}
				}

				if (Mysitewealth_objnode.hasNonNull("Domain_Information")) {
					ArrayNode arrayNode = (ArrayNode) Mysitewealth_objnode.get("Domain_Information");

					if (arrayNode != null) {
						for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
							JsonNode obj = it.next();
							if (obj.hasNonNull("DomainRegistrar")) {
								mainReport.setDomainRegistrar(obj.get("DomainRegistrar").asText());
							}
							if (obj.hasNonNull("RegistrationDate")) {
								mainReport.setDomainRegistrationDate(obj.get("RegistrationDate").asText().trim());
							}
							if (obj.hasNonNull("LastUpdated")) {
								mainReport.setDomainLastUpdated(obj.get("LastUpdated").asText().trim());
							}
							if (obj.hasNonNull("ExpirationDate")) {
								mainReport.setDomainExpirationDate(obj.get("ExpirationDate").asText().trim());
							}
						}
					}
				}

				if (Mysitewealth_objnode.hasNonNull("Domain_Owner_Information")) {
					ArrayNode arrayNode = (ArrayNode) Mysitewealth_objnode.get("Domain_Owner_Information");

					if (arrayNode != null) {
						for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
							JsonNode obj = it.next();
							if (obj.hasNonNull("Name")) {
								mainReport.setDomainName(obj.get("Name").asText());
							}
							if (obj.hasNonNull("Organization")) {
								mainReport.setDomainOrganization(obj.get("Organization").asText().trim());
							}
							if (obj.hasNonNull("Street")) {
								mainReport.setDomainOwnerStreet(obj.get("Street").asText().trim());
							}
							if (obj.hasNonNull("City")) {
								mainReport.setDomainOwnerCity(obj.get("City").asText().trim());
							}
							if (obj.hasNonNull("State")) {
								mainReport.setDomainOwnerState(obj.get("State").asText().trim());
							}
							if (obj.hasNonNull("Postal_Code")) {
								mainReport.setDomainOwnerPostalCode(obj.get("Postal_Code").asText().trim());
							}
							if (obj.hasNonNull("Country")) {
								mainReport.setDomainOwnerCountry(obj.get("Country").asText().trim());
							}
							if (obj.hasNonNull("Fax")) {
								mainReport.setDomainFax(obj.get("Fax").asText().trim());
							}
							if (obj.hasNonNull("Phone")) {
								mainReport.setDomainPhone(obj.get("Phone").asText().trim());
							}
							if (obj.hasNonNull("Email")) {
								mainReport.setDomainOwnerEmail(obj.get("Email").asText().trim());
							}
						}
					}
				}

				if (Mysitewealth_objnode.hasNonNull("Domain_Hosting_Information")) {
					ArrayNode arrayNode = (ArrayNode) Mysitewealth_objnode.get("Domain_Hosting_Information");

					if (arrayNode != null) {
						for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
							JsonNode obj = it.next();
							if (obj.hasNonNull("Sever_IP_Address")) {
								mainReport.setDomainSeverIpAddress(obj.get("Sever_IP_Address").asText());
							}
							if (obj.hasNonNull("Server_Location")) {
								mainReport.setDomainServerLocation(obj.get("Server_Location").asText().trim());
							}
							if (obj.hasNonNull("Hosting_Provider(ISP)")) {
								mainReport.setDomainHostingProvider(obj.get("Hosting_Provider(ISP)").asText().trim());
							}
							if (obj.hasNonNull("Server_HTTP_Code")) {
								mainReport.setDomainServerHttpCode(obj.get("Server_HTTP_Code").asText().trim());
							}
						}
					}
				}

				if (Mysitewealth_objnode.hasNonNull("Web_Server_Information")) {
					ArrayNode arrayNode = (ArrayNode) Mysitewealth_objnode.get("Web_Server_Information");

					if (arrayNode != null) {
						for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
							JsonNode obj = it.next();
							if (obj.hasNonNull("Name_Server")) {
								mainReport.setWebServerName(obj.get("Name_Server").asText());
							}
							if (obj.hasNonNull("IP_Address")) {
								mainReport.setWebServerIPAddress(obj.get("IP_Address").asText().trim());
							}
							if (obj.hasNonNull("Country")) {
								mainReport.setWebServerCountry(obj.get("Country").asText().trim());
							}
						}
					}
				}

				if (Mysitewealth_objnode.hasNonNull("Featured_Stats")) {
					ArrayNode arrayNode = (ArrayNode) Mysitewealth_objnode.get("Featured_Stats");

					if (arrayNode != null) {
						for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
							JsonNode obj = it.next();
							if (obj.hasNonNull("Google_Pagerank")) {
								mainReport.setGooglePagerank(obj.get("Google_Pagerank").asText());
							}
							if (obj.hasNonNull("Page_Speed_Score")) {
								mainReport.setPageSpeedScore(obj.get("Page_Speed_Score").asText().trim());
							}
							if (obj.hasNonNull("Mobile_Speed_Score")) {
								mainReport.setMobileSpeedScore(obj.get("Mobile_Speed_Score").asText().trim());
							}
							if (obj.hasNonNull("Bounce_Rate")) {
								mainReport.setBounceRate(obj.get("Bounce_Rate").asText().trim());
							}
							if (obj.hasNonNull("Moz_Rank")) {
								mainReport.setMozRank(obj.get("Moz_Rank").asText().trim());
							}
							if (obj.hasNonNull("Domain_Authority")) {
								mainReport.setDomainAuthority(obj.get("Domain_Authority").asText().trim());
							}
							if (obj.hasNonNull("Page_Authority")) {
								mainReport.setPageAuthority(obj.get("Page_Authority").asText().trim());
							}
						}
					}
				}

				if (Mysitewealth_objnode.hasNonNull("Website_Safety_Stats")) {
					ArrayNode arrayNode = (ArrayNode) Mysitewealth_objnode.get("Website_Safety_Stats");

					if (arrayNode != null) {
						for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
							JsonNode obj = it.next();
							if (obj.hasNonNull("Google_Safe_Browsing")) {
								mainReport.setGoogleSafeBrowsing(obj.get("Google_Safe_Browsing").asText());
							}
							if (obj.hasNonNull("Spamhaus_Blocklist")) {
								mainReport.setSpamhausBlocklist(obj.get("Spamhaus_Blocklist").asText().trim());
							}
						}
					}
				}
				if (Mysitewealth_objnode.hasNonNull("SEO_Stats")) {
					ArrayNode arrayNode = (ArrayNode) Mysitewealth_objnode.get("SEO_Stats");

					if (arrayNode != null) {
						for (Iterator<JsonNode> it = arrayNode.iterator(); it.hasNext();) {
							JsonNode obj = it.next();
							if (obj.hasNonNull("Total_Backinks")) {
								mainReport.setTotalBackinks(obj.get("Total_Backinks").asText());
							}
							if (obj.hasNonNull("Robots.txt")) {
								mainReport.setRobotsTxt(obj.get("Robots.txt").asText().trim());
							}
							if (obj.hasNonNull("Sitemap.xml")) {
								mainReport.setSitemapXml(obj.get("Sitemap.xml").asText().trim());
							}
						}
					}
				}

			} // if Mysitewealth_objnode end

			List<ProviderReport> providerReportList = new ArrayList<>();

			siteRelatedTrafficCount("http://www.siteprice.org", "siteprice", sitePriceWebsiteReqIP,
					Siteprice_visit_count, providerReportList);
			siteRelatedTrafficCount("https://www.similarweb.com", "similarweb", similarWebsiteReqIP,
					Similarweb_visit_count, providerReportList);
			siteRelatedTrafficCount("http://www.siteworthtraffic.com", "siteworthtraffic", siteWorthWebsiteReqIP,
					Siteworthtraffic_visit_count, providerReportList);
			siteRelatedTrafficCount("https://mysitewealth.com", "mysitewealth", mySiteWealthWebsiteReqIP,
					Mysitewealth_visit_count, providerReportList);
			siteRelatedTrafficCount("https://www.statchest.com", "statchest", StatchestWebsiteReqIP,
					Statchest_visit_count, providerReportList);
			mainReport.setProviderReport(providerReportList);

			return mainReport;
			
		}
			return null;
	}

	private static void siteRelatedTrafficCount(String siteUrl, String siteName, String requestIP, String count,
			List<ProviderReport> providerReportList) {

		ProviderReport providerReport = new ProviderReport();
		providerReport.setSearchSiteUrl(siteUrl);
		providerReport.setSiteName(siteName);
		providerReport.setRequestIP(requestIP);
		providerReport.setTrafficCount(count);
		providerReportList.add(providerReport);
	}
}
