/**
 * 
 */
package com.rage.utils;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import play.Logger;

/**
 * @author k.sathish
 *
 */
public class ErrorPageChecker {

	public ErrorPageChecker() {
	}

	public String errorPageChecking(String site) {

		String msg=null;
		ErrorPageChecker check=new ErrorPageChecker();
		
		ExecutorService executor = Executors.newSingleThreadExecutor();
		Future<?> f1 = executor.submit(() -> {
			return check.checkErrorPageCall(site);
		});
		
		int count=0;
		while (true) {
			count++;
			
			if ((f1.isDone() || f1.isCancelled()) ) {

				System.out.println("-----------Break Executed (websit loaded Success)----------");
				try {msg=(String) f1.get();} catch (Exception e) {}
				break;
			}
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {}
			if(count==10) {
				f1.cancel(true);
				System.out.println("-----------Break Executed (Time out)----------");
				msg="timeout";
				break;
			}
			
		}
		try {
			executor.shutdown();
			executor.awaitTermination(5, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			System.err.println("tasks interrupted");
		}
		
		return msg;
	}
	
	public String checkErrorPageCall(String site) {
		String alert = null;
		String captcha1 = null;
		try {
			Document doc=Jsoup.connect(site).get();
			
			try {alert = doc.select("#alert_text").text();} catch (Exception e) {}
//			try {captcha1 = driver.findElement(By.xpath("//*[contains(text(),'CAPTCHA')]")).getText();} catch (Exception e) {}
//			try {alert = driver.findElement(By.id("alert_text")).getText();} catch (Exception e) {}
//			try {captcha1 = driver.findElement(By.xpath("//*[contains(text(),'CAPTCHA')]")).getText();} catch (Exception e) {}

		} catch (Exception e) {
			Logger.error("ErrorPagechecking method :\n"+e.getMessage());
			System.out.println(e.getMessage());
			return null;
		}
		if(alert.equals(""))
			return null;
		else if(alert !=null)
		return alert;
		/*else if(captcha1.equals(""))
			return null;
		else if(captcha1!=null)
			return captcha1;*/
	
		return null;
	}
	public boolean timeOutCall(WebDriver driver) {
		  IntStream.range(0, 30) .forEach(i ->{ System.out.print(" timeOutCall = "+i); try {
		  Thread.sleep(1000); } catch (InterruptedException e) { } });
		  try { driver.close(); driver.quit(); System.out.println("--- driver qiuted!!!!");}catch (Exception e) { }
		 return true;
	}

}
