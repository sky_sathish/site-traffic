package com.rage.utils;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import play.Logger;

public class Statchest {


	public  List<String> getTrafficCount(String siteName,InetAddress inetAddress) {
		List<String> result=new ArrayList<>();
		siteName = siteName.replaceAll("http://", "");
		siteName = siteName.replaceAll("https://", "");
		siteName = siteName.replaceAll("www.", "");
		String StatchestWebsiteReqIP="";
		String link = "https://www.statchest.com/" + siteName + ".html";
		String DailyUniqueVisitors = "";
		try {
			StatchestWebsiteReqIP=inetAddress.getHostAddress();
			Document doc = Jsoup.connect(link).get();
			Elements els = doc.select(".table-hover").select("tr");
			for (Element ele : els) {
				String title = ele.select("td").text().trim();
				if (title.contains("Daily Unique Visitors")) {
					DailyUniqueVisitors = title.replaceAll("Daily Unique Visitors", "").trim();
					break;
				}
			}
		} catch (Exception e) {	Logger.error("Statchest() ->\n"+e.getMessage());
		}
		// get current proxy ip
		IpAddress ip=new IpAddress();
		String Proxy=ip.getIpAddress();
		if(!Proxy.equals(""))
			StatchestWebsiteReqIP=Proxy;
		
		result.add(DailyUniqueVisitors);
		result.add(StatchestWebsiteReqIP);
		System.out.println("(f5) Statchest done!");
		return result;
	}
}
