package com.rage.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;

import play.Logger;

public class ContactInfo {

	@SuppressWarnings({ "unchecked", "null" })
	public List<Set<String>> sitesSpliter(String siteName, String contactUsLink) {

		System.out.println("contactUsLink : "+contactUsLink);
		ContactInfo contactinfo=new ContactInfo();
		List<Set<String>> phone_email_info=new ArrayList<>();
		
		if(contactUsLink.equals("")) {
		ExecutorService executor = Executors.newWorkStealingPool(3);
		Future<?> f1 = executor.submit(() -> {
			String site=siteName;
			if(site.endsWith("/"))
				site=siteName+"contact";
			else
				site=site+"/"+"contact";
			return contactinfo.webDriverCall(site);
		});

		Future<?> f2 = executor.submit(() -> {
			String site=siteName;
			if(site.endsWith("/"))
				site=site+"contacts";
			else
				site=site+"/"+"contacts";
			return contactinfo.webDriverCall(site);
		});
		
		Future<?> f3 = executor.submit(() -> {
			String site=siteName;
			if(site.endsWith("/"))
				site=site+"contact-us";
			else
				site=site+"/"+"contact-us";
			return contactinfo.webDriverCall(site);
		});

		int count=0;
		while (true) {

			count++;
			if ((f1.isDone() || f1.isCancelled()) && (f2.isDone() || f2.isCancelled())
					&& (f3.isDone() || f3.isCancelled())) {
				break;
			}
			
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {}
			if(count==8) {
				f1.cancel(true);
				f2.cancel(true);
				break;
			}

		}
		try {
			System.out.println("attempt to shutdown executor -> Site '"+siteName+"' is Done!!!");
			executor.shutdown();
			executor.awaitTermination(5, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			System.err.println("tasks interrupted");
		}
		
		if (f1 != null) {
			try {
				phone_email_info.addAll((Collection<? extends Set<String>>) f1.get());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		else if(f2 !=null) {
			try {
				phone_email_info.addAll((Collection<? extends Set<String>>) f2.get());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		else if(f3 !=null) {
			try {
				phone_email_info.addAll((Collection<? extends Set<String>>) f3.get());
			} catch (InterruptedException | ExecutionException e) {
				e.printStackTrace();
			}
		}
		}
		else {
			
			phone_email_info.addAll(contactinfo.webDriverCall(contactUsLink));
		}
		
		System.out.println("(f9) ContactInfo done!");
		return phone_email_info;
	}

	public  List<Set<String>> webDriverCall(String siteName) {
//		System.out.println("site : "+siteName);
		
		List<Set<String>> phone_email_info=new ArrayList<>();
		try {
			Document doc = Jsoup.connect(siteName.trim()).get();
		
		Elements els=doc.body().getAllElements();
		
		phone_email_info.add(getPhoneInfowithPattern(doc));
		phone_email_info.add(getEmailwithPattern(els.toString()));

		
		}catch(Exception e) {
			Logger.error("ContactInfo->webDriverCall() ->\n"+e.getMessage());
			System.out.println(e);}
		return phone_email_info;
	}

	public  Set<String> getPhoneInfowithPattern(Document passingDoc) {

		Set<String> phoneInfo = new HashSet<>();

		try {
		Document doc = passingDoc;
		
		Elements els=doc.body().getAllElements();
		for(Element ele:els) {
		if(ele.hasText()) {
		
		String phonePattern = "(" + "[+0-9]*\\s[(][0-9]{3}[)]\\s[0-9]*[-][0-9]*|" // +1 (877) 576-3636
				+ "[(][0-9]{3}[)]\\s[0-9]{3}[-][0-9]{3}|" // (877) 576-363
				+ "[(][0-9]{3}[)]\\s[0-9]{3}\\s[0-9]{3}|" // (877) 576 363
				+ "[0-9]{3}\\s[0-9]{3}[-][0-9]{3}|" // 877 576-363
				+ "[0-9]*\\s[-]\\s[0-9]*|" // 38498000 - 021
				+ "[+0-9]*[-][0-9]{3}[-][0-9]{4}|" // +877-591-9571
				+ "[+0-9]*[-][0-9]*[-][0-9]*[-][0-9]*|" // +1-323-389-9269
				+ "[0-9]*[-][0-9]*[-][0-9]*[-][0-9]*|" // 1-800-444-3353
				+ "[+0-9]{3}\\s[0-9]{5}\\s[0-9]{5}|" // +91 12345 67890
				+ "[+0-9]*\\s[0-9]{5}[-][0-9]{5}|" // +91 12345-67890
				+ "[+0-9]{3}\\s[(][0-9]{1}[)]\\s[0-9]*\\s[0-9]*|" // +44 (0) 1446 688111
				+ "[+0-9]{3}\\s[0-9]{4}\\s[0-9]{6}" // +44 1235 529449

				+ ")";

		Pattern p = Pattern.compile(phonePattern);
		Matcher m = p.matcher(ele.text());

		while (m.find()) {

			if (!m.group().isEmpty() && !m.group().trim().equals("") 
					&& m.group().length() >= 10 && !m.group().startsWith("-")
					&& !m.group().endsWith("-")) {
				phoneInfo.add(m.group().trim());
			}

		}
		}
	}
		}catch(Exception e) {
			Logger.error("ContactInfo->getPhoneInfowithPattern() ->\n"+e.getMessage());
		}
		return phoneInfo;

	}

	public  Set<String> getEmailwithPattern(String source) {

		Set<String> emailInfo = new HashSet<>();

		try {
		String emailPattern = "[a-zA-Z0-9._]*@[a-zA-Z]*[.a-z]*";

		Pattern p = Pattern.compile(emailPattern);
		Matcher m = p.matcher(source);

		while (m.find()) {

			if (!m.group().isEmpty() && !m.group().trim().equals("")
					&& !m.group().startsWith("@") && !m.group().endsWith("@")
					&& !m.group().endsWith("example.com") && !m.group().toLowerCase().trim().startsWith("nr@")) {
				emailInfo.add(m.group());
			}

		}
		}catch(Exception e) {
			Logger.error("ContactInfo->getEmailwithPattern() ->\n"+e.getMessage());
		}
		return emailInfo;
	}

}
