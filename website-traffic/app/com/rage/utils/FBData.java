package com.rage.utils;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import play.Logger;

public class FBData {

	public static void main(String[] args) {
		getFBFollowersCount("flipkart");
	}

	public static String getFBFollowersCount(String siteName) {
		String link = "https://www.facebook.com/" + siteName;
		String Followers = "";
		try {
			Document doc = Jsoup.connect(link).get();
			Elements els = doc.select("._4bl9");
			for (Element ele : els) {
				Followers = ele.text().trim();
				if (Followers.toLowerCase().contains("follow ")) {
					break;
				}
			}
		} catch (Exception e) {Logger.error("getFBFollowersCount() ->\n"+e.getMessage());
		}
		System.out.println("(f7) Fb Followers done!");
		return Followers;
	}
}
