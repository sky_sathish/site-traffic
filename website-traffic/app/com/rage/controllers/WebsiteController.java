package com.rage.controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.commons.io.FileUtils;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.rage.models.csv.Csv;
import com.rage.models.csv.service.CsvService;
import com.rage.models.report.main.MainReport;
import com.rage.models.report.service.MainReportService;
import com.rage.models.website.Website;
import com.rage.models.website.csv.mapping.CsvWebsiteMapping;
import com.rage.models.website.csv.mapping.service.CsvWebsiteMappingService;
import com.rage.models.website.service.WebsiteService;
import com.rage.utils.RootClass;

import io.ebean.SqlRow;
import play.Logger;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;

public class WebsiteController extends Controller {

	@Inject
	public WebsiteService websiteServ;

	@Inject
	public CsvService csvServ;

	@Inject
	public CsvWebsiteMappingService csvWebsiteMappingServ;

	@Inject
	public MainReportService mainReportServ;

	public Result sayHi() {
		return ok();
	}

	public Result addWebsites() throws IOException {
		Http.MultipartFormData<File> body = request().body().asMultipartFormData();
		Http.MultipartFormData.FilePart<File> csvFile = body.getFile("csvFile");
		Date date = new Date();
		List<Csv> csvList = csvServ.getCsvList();
		String fName = csvFile.getFilename();
		int lastIndex = fName.lastIndexOf(".");
		String fileName = fName.substring(0, lastIndex);
		String isCsvFileName=fName.substring(lastIndex,fName.length());

		if (csvFile != null && !fName.isEmpty() && isCsvFileName.equals(".csv")) {
			File file = csvFile.getFile();
			FileUtils.copyFile(file, new File("public/uploadFile", fileName + "" + date.getTime() + ".csv"));
			List<Website> websiteList = csvFileRead(file);

			Csv csvDetails = setCsvData(date, fileName);

			Thread t=new Thread(() -> {
				List<SqlRow>  websiteUrlLst = null;
				List<CsvWebsiteMapping> csvWebsiteList =null;
				
				websiteUrlLst = websiteServ.addWebsiteUrl(websiteList,csvDetails);
				websiteList.clear();
				csvWebsiteList= setCsvWebsiteMappingData(websiteUrlLst, csvDetails);
				websiteUrlLst.clear();
				csvWebsiteMappingServ.addCsvWebsiteMapping(csvWebsiteList);
				csvWebsiteList.clear();
				reportGenerate(csvDetails);
				
			});
			t.start();

			if (csvDetails != null && csvDetails.getId() != null && !(csvDetails.isReportGenerated())) {
				ObjectNode resp = Json.newObject();
				resp.put("msg", "Report generating please wait ...");
				resp.put("csvId", csvDetails.getId().toString());
				flash("info", resp.toString());
				return ok(com.rage.views.html.upload.render(Json.toJson(csvList)));
			} else {
				flash("error", "File upload failed");
				return ok(com.rage.views.html.upload.render(null));
			}
			
		} else {
			flash("error", "File upload failed");
			return ok(com.rage.views.html.upload.render(null));
		}
		
	}

	private void reportGenerate(Csv csvDetails) {
//		Csv latestCsvData = csvServ.getCsvDetailById(csvDetails.getId().toString());
		List<SqlRow> csvWebsiteMapping = csvWebsiteMappingServ
				.getCsvWebsiteMapping(csvDetails.getId().toString());
		if(csvWebsiteMapping!=null && !csvWebsiteMapping.isEmpty()) {
			
			AtomicInteger siteDoneCount=new AtomicInteger(0);
			ExecutorService executor = Executors.newWorkStealingPool(8);
			csvWebsiteMapping.forEach(website -> {
			
			if((website.getString("url")!=null && !"".equals(website.getString("url"))) && 
					website.getString("id")!=null && !"".equals(website.getString("id"))) {
				
					executor.execute(()->{
				
					Long id=website.getLong("id");
					String url=website.getString("url");

					MainReport data = RootClass.getData(id,url);
						if(data!=null) {
							data.setCsvId(csvDetails.getId());
							mainReportServ.addMainReport(data);
							int noOfCount=siteDoneCount.incrementAndGet();
							int TotalSiteCount=csvWebsiteMapping.size();
							Logger.info("------------------- Site-Traffic project running task ("+noOfCount+"/"+TotalSiteCount+") "+(noOfCount*100)/TotalSiteCount+"% completed.------------------------");
						}
				});	
						
			}
			
		});
		
			try {
				executor.shutdown();
				while(!executor.isTerminated()) {
				}
				
				executor.awaitTermination(5, TimeUnit.SECONDS);
			} catch (InterruptedException e) {
				e.printStackTrace();
				executor.shutdown();
			}
		csvServ.updateCsvReportStatusUsingId(csvDetails.getId().toString());
		
		}
		else
		{
			csvServ.updateCsvReportStatusUsingId(csvDetails.getId().toString());
		}
		
	}

	private List<CsvWebsiteMapping> setCsvWebsiteMappingData(List<SqlRow> websiteUrlLst, Csv csvDetails) {
		List<CsvWebsiteMapping> csvWebsiteList = new ArrayList<>();
		
		if(websiteUrlLst !=null && !websiteUrlLst.isEmpty()) {
		websiteUrlLst.forEach(website -> {
			CsvWebsiteMapping csvWebsiteMapping = new CsvWebsiteMapping();
			if (website.getLong("id") != null) {
				if (csvDetails.getId() != null) {
					csvWebsiteMapping.setCsvId(csvDetails.getId());
					csvWebsiteMapping.setWebsiteId(website.getLong("id"));
					csvWebsiteList.add(csvWebsiteMapping);
				}
			}
			
		});
		return csvWebsiteList;
		}
		else {
		return null;
		}
	}

	private Csv setCsvData(Date date, String fileName) {
		Csv csv = new Csv();
		csv.setFileName(fileName + "" + date.getTime() + ".csv");
		csv.setCreatedTime(date);
		csv.setReportGenerated(false);
		Csv csvDetails = csvServ.addCsvDetail(csv);
		return csvDetails;
	}

	private List<Website> csvFileRead(File file) throws FileNotFoundException, IOException {
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line = null;
		Scanner scanner = null;
		int index = 0;
		List<Website> websiteList = new ArrayList<>();
		while ((line = reader.readLine()) != null) {
			Website website = new Website();
			scanner = new Scanner(line);
			scanner.useDelimiter(",");
			while (scanner.hasNext()) {
				String data = scanner.next();
				if (index == 0)
					website.setUrl(data);
				else if (index == 1)
					website.setAlexaRanking(data);
				else if (index == 2)
					website.setCountryIp(data);
				else if (index == 3)
					website.setCountryCodePage(data);
				else if (index == 4)
					website.setIp(data);
				else if (index == 5)
					website.setWebServer(data);
				else if (index == 6)
					website.setEmail(data);
				else if (index == 7)
					website.setPhone(data);
					
				index++;
			}
			index = 0;
			websiteList.add(website);
		}
		reader.close();
		return websiteList;
	}

	public Result getWebsiteByUrl() {
		String url = request().getQueryString("url");
		Website websiteUrl = websiteServ.getWebsiteByUrl(url);
		return ok(com.rage.views.html.index.render(null,null,null,null,null,null));
	}

	public Result uploadCsvFile() throws IOException {
		Http.MultipartFormData<File> body = request().body().asMultipartFormData();
		Http.MultipartFormData.FilePart<File> csvFile = body.getFile("csvFile");
		if (csvFile != null) {
			String fileName = csvFile.getFilename();
			File file = csvFile.getFile();
			FileUtils.moveFile(file, new File("public/uploadFile", fileName));
			return ok("File uploaded");
		} else {
			flash("error", "Missing file");
			return ok("Missing file");
		}
	}

	public Result getWebsiteResult() {
		Csv latestCsvData = csvServ.getLatestCsvDetails();
		List<MainReport> mainReportList = new ArrayList<>();
		if (latestCsvData != null && latestCsvData.getId() != null) {
			String csvId = latestCsvData.getId().toString();
			
			List<MainReport> mainReportLst = mainReportServ.getMainReport(csvId,0,100);
			if (mainReportLst != null) {
				mainReportLst.forEach(mainReport -> {
					if (mainReport != null && mainReport.getSiteId() != null) {
						Website website = websiteServ.getWebsiteById(mainReport.getSiteId());
						mainReport.setWebsite(website);
					}
				});
				mainReportList.addAll(mainReportLst);
			}
			if (mainReportList != null) {
				return ok(com.rage.views.html.index.render(csvId,"latest",1,1,5,Json.toJson(mainReportList)));
			}
		}
		return ok(com.rage.views.html.index.render(null,null,null,null,null,null));
	}

	public Result uploadCsv() {
		List<Csv> csvList = csvServ.getCsvList();
		if (csvList != null && !csvList.isEmpty()) {
			return ok(com.rage.views.html.upload.render(Json.toJson(csvList)));
		} else {
			return ok(com.rage.views.html.upload.render(null));
		}
	}
}
