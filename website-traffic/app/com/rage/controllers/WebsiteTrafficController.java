/**
 * 
 */
package com.rage.controllers;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import com.rage.models.csv.Csv;
import com.rage.models.csv.service.CsvService;
import com.rage.models.report.main.MainReport;
import com.rage.models.report.service.MainReportService;
import com.rage.models.website.Website;
import com.rage.models.website.csv.mapping.CsvWebsiteMapping;
import com.rage.models.website.csv.mapping.service.CsvWebsiteMappingService;
import com.rage.models.website.service.WebsiteService;
import com.rage.utils.RootClass;

import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

/**
 * @author neethithevan.r
 *
 */
public class WebsiteTrafficController extends Controller {

	private static final int REPORT_SHOW_RECORD_COUNT = 5000;
	@Inject
	public CsvService csvServ;

	@Inject
	public MainReportService mainReportServ;

	@Inject
	public CsvWebsiteMappingService csvWebsiteMappingServ;

	@Inject
	public WebsiteService websiteServ;

	@SuppressWarnings("unchecked")
	public Result findWebsiteTraffic() throws InterruptedException, ExecutionException {
		/*
		 * Csv latestCsvData = csvServ.getLatestCsvDetails();
		 * List<CsvWebsiteMapping> csvWebsiteMapping = csvWebsiteMappingServ
		 * .getCsvWebsiteMapping(latestCsvData.getId().toString()); List<Website>
		 * websiteUrlList = new ArrayList<>(); csvWebsiteMapping.forEach(website -> { if
		 * (website != null && website.getWebsite() != null &&
		 * !website.getWebsite().getUrl().isEmpty()) {
		 * websiteUrlList.add(website.getWebsite()); } }); if (websiteUrlList != null &&
		 * !websiteUrlList.isEmpty()) { List<MainReport> mainReportList =
		 * RootClass.getData(websiteUrlList); if (!mainReportList.isEmpty()) {
		 * mainReportList.forEach(mainReport -> {
		 * mainReport.setCsvId(latestCsvData.getId());
		 * mainReportServ.addMainReport(mainReport); }); } return
		 * ok(Json.toJson(mainReportList)); }
		 */
		return notFound();
	}

	public Result findWebsiteTrafficWithCsvId(String csvId) {
		
//		int totalRecodCount = mainReportServ.getMainReportTotalcount(csvId);
//		List<MainReport> mainReportList = mainReportServ.getMainReport(csvId, 0, 100);
//		if (mainReportList != null) {
//			int pages = 1;
//			if (totalRecodCount > 10000) {
//				pages = totalRecodCount / 10000;
//			}
//			return ok(com.rage.views.html.index.render(csvId,"old",totalRecodCount, 1, pages, Json.toJson(mainReportList)));
//		} else {
//			return ok(com.rage.views.html.index.render(null,null,null, null, null, null));
//		}
		
		return showOldData(csvId,1);
	}

	public Result home() {

		Csv latestCsvData = csvServ.getLatestCsvDetails();
		List<MainReport> mainReportList = new ArrayList<>();
		if (latestCsvData != null && latestCsvData.getId() != null) {
			String csvId = latestCsvData.getId().toString();
			Integer fromIndex = 0, toIndex = REPORT_SHOW_RECORD_COUNT;
			int totalRecodCount = mainReportServ.getMainReportTotalcount(csvId);
			List<MainReport> mainReportLst = mainReportServ.getMainReport(csvId, fromIndex, toIndex);
			if (mainReportLst != null) {
				mainReportLst.forEach(mainReport -> {
					if (mainReport != null && mainReport.getSiteId() != null) {
						Website website = websiteServ.getWebsiteById(mainReport.getSiteId());
						mainReport.setWebsite(website);
					}
				});
				mainReportList.addAll(mainReportLst);
				if (mainReportList != null) {
					int pages = 1;
					if (totalRecodCount > REPORT_SHOW_RECORD_COUNT) {
						pages = Math.round((float)totalRecodCount / (float)REPORT_SHOW_RECORD_COUNT);
					}
					System.gc();
					return ok(com.rage.views.html.index.render(csvId,"latest",totalRecodCount, 1, pages, Json.toJson(mainReportList)));
				}
			}
		}
		return ok(com.rage.views.html.index.render(null,null,null, null, null, null));
	}

	public Result showLatestData(String csv_id,Integer currentPage) {
		// get current page
		int getCurrentPage = currentPage;

		Csv latestCsvData = csvServ.getLatestCsvDetails();
		List<MainReport> mainReportList = new ArrayList<>();
		if (latestCsvData != null && latestCsvData.getId() != null) {
			String csvId = latestCsvData.getId().toString();

			Integer fromIndex = ((getCurrentPage - 1) * REPORT_SHOW_RECORD_COUNT);
			Integer toIndex = REPORT_SHOW_RECORD_COUNT;
			int totalRecodCount = mainReportServ.getMainReportTotalcount(csvId);
			List<MainReport> mainReportLst = mainReportServ.getMainReport(csvId, fromIndex, toIndex);
			if (mainReportLst != null) {
				mainReportLst.forEach(mainReport -> {
					if (mainReport != null && mainReport.getSiteId() != null) {
						Website website = websiteServ.getWebsiteById(mainReport.getSiteId());
						mainReport.setWebsite(website);
					}
				});
				mainReportList.addAll(mainReportLst);
				if (mainReportList != null) {
					int pages = 1;
					if (totalRecodCount > REPORT_SHOW_RECORD_COUNT) {
						pages = Math.round((float)totalRecodCount / (float)REPORT_SHOW_RECORD_COUNT);
					}
					System.gc();
					return ok(com.rage.views.html.index.render(csvId,"latest",totalRecodCount, getCurrentPage, pages,
							Json.toJson(mainReportList)));
				}
			}
		}
		return ok(com.rage.views.html.index.render(null,null,null, null, null, null));
	}

	public Result showOldData(String csv_id, Integer currentPage) {

		if (csv_id != null && csv_id != "") {
			int totalRecodCount = mainReportServ.getMainReportTotalcount(csv_id);
			String type = "old";
			Csv latestCsvData = csvServ.getLatestCsvDetails();
			if((latestCsvData != null && latestCsvData.getId() !=null)&&(latestCsvData.getId().toString().trim().equals(csv_id.trim()))) {
				type = "latest";
			}
			Integer fromIndex = ((currentPage - 1) * REPORT_SHOW_RECORD_COUNT);
			Integer toIndex = REPORT_SHOW_RECORD_COUNT;
			List<MainReport> mainReportList = mainReportServ.getMainReport(csv_id,fromIndex,toIndex);
			if (mainReportList != null) {
				
				int pages = 1;
				if (totalRecodCount > REPORT_SHOW_RECORD_COUNT) {
					pages = Math.round((float)totalRecodCount / (float)REPORT_SHOW_RECORD_COUNT);
				}
				System.gc();
				return ok(com.rage.views.html.index.render(csv_id,type,totalRecodCount, currentPage, pages, Json.toJson(mainReportList)));
			} else {
				return ok(com.rage.views.html.index.render(null,null,null, null, null, null));
			}

		} else {
			return showLatestData(csv_id,1);
		}
	}

	public Result contactInfo() {
		String id = request().getQueryString("id");
		ObjectNode res = Json.newObject();
		if (id != null && id.trim().length() > 0) {

			Website website = websiteServ.getWebsiteById(Long.parseLong(id));
			if (website != null) {
				res.put("phone", website.getPhone());
				res.put("email", website.getEmail());
				res.put("success", true);
				return ok(res);
			} else {
				res.put("success", false);
				res.put("response", "Resource not found");
				return ok(res);
			}
		}
		res.put("success", false);
		res.put("response", "Resource not found");
		return ok(res);
	}

	public Result contactInfoUpdate() {
		Map<String, String[]> data = request().body().asFormUrlEncoded();

		ObjectNode res = Json.newObject();
		if (data != null && !data.isEmpty() && data.get("id") != null && data.get("id")[0] != null
				&& data.get("phone") != null && data.get("phone")[0] != null && data.get("email") != null
				&& data.get("email")[0] != null) {
			String id = data.get("id")[0];
			String phone = data.get("phone")[0];
			String email = data.get("email")[0];
			Website website = new Website();
			website.setPhone(phone);
			website.setEmail(email);
			boolean isUpdateWebsite = websiteServ.updateWebsite(id, website);
			if (isUpdateWebsite) {
				res.put("success", true);
				res.put("msg", "Updated successfully");
				return ok(res);
			} else {
				res.put("success", false);
				res.put("msg", "Updated unsuccessfully");
				return ok(res);
			}
		} else {
			res.put("success", false);
			res.put("msg", "Invalid data");
			return ok(res);
		}
	}
}
