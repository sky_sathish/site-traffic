# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table csv (
  id                            bigint auto_increment not null,
  file_name                     varchar(255),
  created_time                  datetime,
  is_report_generated           tinyint(1) default 0 not null,
  constraint pk_csv primary key (id)
);

create table csv_website_mapping (
  id                            bigint auto_increment not null,
  csv_id                        bigint,
  website_id                    bigint,
  constraint pk_csv_website_mapping primary key (id)
);

create table main_report (
  id                            bigint auto_increment not null,
  csv_id                        bigint,
  site_id                       bigint,
  site_url                      varchar(255),
  has_fb_page                   varchar(255),
  has_twitter_page              varchar(255),
  fb_followers                  varchar(255),
  twitter_followers             varchar(255),
  fb_url                        LONGTEXT,
  twitter_urll                  LONGTEXT,
  daily_unique_visitors         varchar(255),
  daily_revenue                 varchar(255),
  daily_unique_pageviews        varchar(255),
  monthly_unique_visitors       varchar(255),
  monthly_revenue               varchar(255),
  monthly_unique_pageviews      varchar(255),
  yearly_unique_visitors        varchar(255),
  yearly_revenue                varchar(255),
  yearly_unique_pageviews       varchar(255),
  global_rank                   varchar(255),
  country_rank                  varchar(255),
  country_label                 varchar(255),
  website_load_time             varchar(255),
  website_page_size             varchar(255),
  domain_name                   varchar(255),
  domain_organization           varchar(255),
  domain_fax                    varchar(255),
  domain_phone                  varchar(255),
  domain_owner_street           varchar(255),
  domain_owner_city             varchar(255),
  domain_owner_state            varchar(255),
  domain_owner_postal_code      varchar(255),
  domain_owner_country          varchar(255),
  domain_owner_email            varchar(255),
  domain_registration_date      varchar(255),
  domain_last_updated           varchar(255),
  domain_expiration_date        varchar(255),
  domain_registrar              varchar(255),
  domain_sever_ip_address       varchar(255),
  domain_server_location        varchar(255),
  domain_hosting_provider       varchar(255),
  domain_server_http_code       varchar(255),
  web_server_name               varchar(255),
  web_server_ip_address         varchar(255),
  web_server_country            varchar(255),
  google_pagerank               varchar(255),
  page_speed_score              varchar(255),
  mobile_speed_score            varchar(255),
  bounce_rate                   varchar(255),
  moz_rank                      varchar(255),
  domain_authority              varchar(255),
  page_authority                varchar(255),
  google_safe_browsing          varchar(255),
  spamhaus_blocklist            varchar(255),
  total_backinks                varchar(255),
  robots_txt                    varchar(255),
  sitemap_xml                   varchar(255),
  phone_info                    LONGTEXT,
  email_info                    LONGTEXT,
  constraint pk_main_report primary key (id)
);

create table provider_report (
  id                            bigint auto_increment not null,
  search_site_url               varchar(255),
  site_name                     varchar(255),
  request_ip                    varchar(255),
  traffic_count                 varchar(255),
  main_report_id                bigint,
  constraint pk_provider_report primary key (id)
);

create table website (
  id                            bigint auto_increment not null,
  url                           varchar(255),
  alexa_ranking                 varchar(255),
  country_ip                    varchar(255),
  country_code_page             varchar(255),
  ip                            varchar(255),
  web_server                    varchar(255),
  phone                         varchar(255),
  email                         varchar(255),
  constraint uq_website_url unique (url),
  constraint pk_website primary key (id)
);

create table website_temp (
  id                            bigint auto_increment not null,
  csv_id                        bigint,
  url                           varchar(255),
  alexa_ranking                 varchar(255),
  country_ip                    varchar(255),
  country_code_page             varchar(255),
  ip                            varchar(255),
  web_server                    varchar(255),
  phone                         varchar(255),
  email                         varchar(255),
  constraint pk_website_temp primary key (id)
);

create index ix_csv_website_mapping_csv_id on csv_website_mapping (csv_id);
alter table csv_website_mapping add constraint fk_csv_website_mapping_csv_id foreign key (csv_id) references csv (id) on delete restrict on update restrict;

create index ix_csv_website_mapping_website_id on csv_website_mapping (website_id);
alter table csv_website_mapping add constraint fk_csv_website_mapping_website_id foreign key (website_id) references website (id) on delete restrict on update restrict;

create index ix_main_report_site_id on main_report (site_id);
alter table main_report add constraint fk_main_report_site_id foreign key (site_id) references website (id) on delete restrict on update restrict;

create index ix_provider_report_main_report_id on provider_report (main_report_id);
alter table provider_report add constraint fk_provider_report_main_report_id foreign key (main_report_id) references main_report (id) on delete restrict on update restrict;


# --- !Downs

alter table csv_website_mapping drop foreign key fk_csv_website_mapping_csv_id;
drop index ix_csv_website_mapping_csv_id on csv_website_mapping;

alter table csv_website_mapping drop foreign key fk_csv_website_mapping_website_id;
drop index ix_csv_website_mapping_website_id on csv_website_mapping;

alter table main_report drop foreign key fk_main_report_site_id;
drop index ix_main_report_site_id on main_report;

alter table provider_report drop foreign key fk_provider_report_main_report_id;
drop index ix_provider_report_main_report_id on provider_report;

drop table if exists csv;

drop table if exists csv_website_mapping;

drop table if exists main_report;

drop table if exists provider_report;

drop table if exists website;

drop table if exists website_temp;

